# Landing page copy

## Site header

### Title and subtitle

#### Option 1

**Title:** Learn everything you need to know about Free Software and how to become a confident contributor

**Subtitle:** Develop new skills and career-changing expertise with this hands-on free course

#### Option 2

**Title:** Open Source Contributor Masterclass: How to confidently contribute to FLOSS

**Subtitle:** Understand what FLOSS is, and learn the expertise to make contributions to open source software

#### Option 3

**Title:** A MOOC dedicated to Free/Libre/Open Source Software

**Subtitle:** Learn about free open source software, how it works, and how it’s changing the world around us. All for free!

#### Option 4

**Title:** A free MOOC about FLOSS

**Subtitle:** Teaching you everything you need to know about Free/Libre/Open Source Software

## About this course

We’re passionate about free open source software. This course is a collection of the skills and expertise contributors to this course have acquired over years of working on Free/Libre/Open Source Software (FLOSS) projects. We want to share this experience with you to give you a headstart on your journey into the world of open source. 

### Course description

By the end of this 7-module free course, you will not only be able to contribute confidently to FLOSS projects, but will also have a much deeper understanding of the importance of the “open source” way of working.

We will start by defining free software, discussing its benefits, and investigating the vital role it plays in society. Free software has a strong community foundation, so we will introduce you to the open source community right from the start. We will also teach you about how these communities tend to operate, as well as the different types of FLOSS contributors.

You will learn about the tools and methods required to work on FLOSS projects, as well as the best practices for collaborating on open source projects. We will dive into the technical aspects of testing, code reviews, code analysis, and build techniques. With these skills, you will be able to become an active and successful member of the open source community. 

We will introduce you to the different licensing models that are available, and discuss the pros and cons of each. We will also investigate the various economic models that can be applied to FLOSS projects, and introduce you to various successful organizations, charities, and high-profile figures in the open-source community.

We believe that we learn best by doing, so we have designed this course with a focus on hands-on experience. At the end of the course, your task will be to create a successful commit on an existing open source software project.

### What you will learn 

* Getting pull requests merged on third party open source software projects
* How to become a valuable member of the open source community 
* Best practices for cooperating and collaborating effectively
* The tools and methods to work on FLOSS projects
* Wisdom from high-profile figures in the open-source community 
* FLOSS licensing and monetization models

## Who the course is for

This course is for programmers who are interested in learning more about open source software and the FLOSS community. It is not only great if you're still learning about the programming world, but also if you're a developer whose experience lies mainly in writing proprietary code.

Even experienced developers can find it intimidating to contribute to open source projects. This isn’t surprising; it’s daunting to contribute a change upstream, and contributing effectively takes a lot of effort and skill. This course aims to give you the expertise you need to make contributions to FLOSS projects confidently and effectively.

## Why FLOSS?

FLOSS has proven to be an efficient way to create quality, reliable software, and we believe it has the power, not only to improve society, but to change the world. 

FLOSS is built on the principles of freedom, collaboration, and community. It stands for “Free/Libre/Open Source Software” and is built on these four essential freedoms:

1. The freedom to run software as you wish, for any purpose
1. The freedom to study how the software works, and adapt it to your needs
1. The freedom to redistribute copies of the software to help others 
1. The freedom to distribute copies of your modified versions to others, for the benefit of the community

So when you hear the “free” in FLOSS, think of “freedom” and “liberty”; don’t think of “free coffee”! 

We believe that FLOSS is a better way to create software by allowing varied actors to pull their efforts together and collaborate in a way that is respectful of the user's rights. Well managed, the end result is better than anyone can achieve on their own, with less duplication. But contributing and collaborating to free software requires a specific approach and set of skills, which is still misunderstood and rare today. That’s why we’re designing this course - we believe it is the way of the future, and we can't wait to see more people join the movement and experience it for themselves. 

## Who we are 

### Involved institutions

* [Framasoft][framasoft]: A popular education nonprofit in France, focused on promotion, dissemination and development of free software

* [OpenCraft][opencraft]: A FLOSS provider and one of the main contributors to the [Open edX][openedx] project

* [OpenStack Upstream Institute][openstack-upstream]: A training program about the different ways of contributing to the OpenStack project

### Our supporters

This MOOC is produced by IMT and Télécom Paris with the financial support of the Patrick and Lina Drahi Foundation.

* [IMT][imt]
* [OpenCraft][opencraft]
* [Open edX][openedx]
* [OpenStack][openstack]
* [Télécom Paris][télécom-paris]
* [Patrick and Lina Drahi Foundation][pldf]

### Course contributors

We’re a passionate group of open source experts who come from a multi-faceted background. We are excited to share our years of experience working on FLOSS projects with you.

* Marc Jeanmougin: Research engineer at [Télécom Paris][télécom-paris] and a contributor to free software such as [Inkscape][inkscape].

* Rémi Sharrock: Professor at [Télécom Paris][télécom-paris]

* Xavier Antoviaque: Founder and CEO at [OpenCraft][opencraft]

* Geoffrey L: Fullstack software engineer at [OpenCraft][opencraft]

* Jillian Vogel: Senior open source developer at [OpenCraft][opencraft] 

* Olivier Berger: Research engineer at [IMT][imt]

* Anna Khazina: Digital learning project manager at [IMT][imt]

* Loïc Dachary: Free software developer

* Polyèdre: Computer science student

## Stay in touch

We are still building the course but would love to keep you updated on our progress! Please add your email address below so we can keep in touch. 

## Want to get involved?

If you’d like to contribute to the development of this course, please [view our project on GitLab][mooc-floss]. Feel free to use our [issues list][mooc-floss-issues] to ask questions about the project, help with existing issues, or raise new issues.

<!-- LINKS -->

[télécom-paris]: https://www.telecom-paris.fr
[framasoft]: https://framasoft.org
[opencraft]: https://opencraft.com/
[inkscape]: https://inkscape.org/
[openedx]: https://open.edx.org/
[openstack]: https://www.openstack.org/
[openstack-upstream]: https://docs.openstack.org/upstream-training/
[pldf]: https://plfa.info/
[mooc-floss]: https://gitlab.com/mooc-floss/mooc-floss
[mooc-floss-issues]: https://gitlab.com/mooc-floss/mooc-floss/-/issues
[imt]: https://www.imt.fr/
