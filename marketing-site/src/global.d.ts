/// <reference types="@sveltejs/kit" />

/**
 * SvelteKit SVG Plugin
 */
declare module '*.svg?src' {
  const content: string;
  export default content;
}

declare module '*.svg?component' {
  const content: string;
  export default content;
}

declare module '*.svg?url' {
  const content: string;
  export default content;
}
// SvelteKit SVG Plugin

declare namespace svelte.JSX {
  /* eslint-disable-next-line */
  interface HTMLAttributes<T> {
    /* $lib/actions/intersection-observer */
    onintersectionentry?: (event: CustomEvent<number> & {target: EventTarget & T}) => void;
    onintersectionexit?: (event: CustomEvent<number> & {target: EventTarget & T}) => void;
  }
}

declare module '@glidejs/glide/dist/glide.modular.esm';
