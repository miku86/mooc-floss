import {IconId} from './enums';

import Icon from './index.svelte';

export {Icon, IconId};
