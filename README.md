# MOOC FLOSS

* Website: https://mooc-floss.gitlab.io/mooc-floss/
* Course (draft/WIP): https://courses.opencraft.com/courses/course-v1:MOOC-FLOSS+101+dev/course/

## Hi and Welcome!

This repository holds what will soon become a <abbr title="Massive Open Online Course">MOOC</abbr> about contributing to Free, Libre, and Open Source Software (FLOSS). However, because everyone loves a bit of meta, this MOOC sees itself as a Free, Libre, and open source project, published with a CC-BY-SA license! We'll also make sure to use only FLOSS tools to make the content of this MOOC.

This license and organization will allow to respect the principles outlined in the [UNESCO Open Educational Resources](https://unesdoc.unesco.org/ark:/48223/pf0000157987.locale=fr) framework ([Guidelines](https://unesdoc.unesco.org/ark:/48223/pf0000213605)), aiming to produce _"materials used to support education that may be freely accessed, reused, modified, and shared"_ (UNESCO words, not ours). If you are familiar with the Free Software definitions, you might notice some similarity.


As a first version, this repository is roughly organized as follows:

```
.
├── README.md
├── syllabus
│   └── README.md
├── W1-What
│   ├── README.md
│   ├── assets
│   │   └── logo.svg
│   ├── brainstorm.md
│   └── pages
├── W2-Where
…
```

Tentative logic for organizing stuff:

 - Each folder should have a README.md file, describing its contents.

 - The global organization of the MOOC is described in the [syllabus/](syllabus/README.md) folder.

 - Each week should contain its own assets.

 - The `pages/` folder will contain the actual content of the MOOC, that will
   be visible to the students on the platform. The plan is to write it in
   Markdown, then generate a page with e.g. Hugo in the CI and embed (as an
   iframe) the corresponding gitlab.io page in the MOOC platform.

 - Feel free to open an issue or a <abbr title="Merge Request" >MR</abbr> for all questions or suggestions ☺

 - There is no such thing as "Too many references"



## Team and sponsors

> This section **needs expansion**. You can help by [adding to it](https://gitlab.com/mooc-floss/mooc-floss/-/merge_requests)

 * Marc Jeanmougin is a research engineer at [Télécom Paris](https://www.telecom-paris.fr) and a contributor to free software such as Inkscape.
 * Rémi Sharrock is a professor at [Télécom Paris](https://www.telecom-paris.fr)
 * [Framasoft](https://framasoft.org) is a popular education nonprofit in
   France, focused on promotion, dissemination and development of free software.
 * [OpenCraft](https://opencraft.com/), a FOSS provider and one of the main contributors to the Open edX project.
 * [OpenStack Upstream Institute](https://docs.openstack.org/upstream-training/), a training program to share knowledge about the different ways of contributing to the OpenStack project.

This MOOC is produced by IMT and Telecom Paris with the financial support of the Patrick and Lina Drahi Foundation

## Feedback and contact

The main point where to submit feedback, ideas, or point out errors would be the [gitlab repository](https://gitlab.com/mooc-floss/mooc-floss) where you should feel free to submit issues, even for questions about the project, or MRs to improve or fix issues. Please use English here.

For informal discussions, we have a [matrix channel](https://matrix.to/#/!iyOZfbqdleCKaUkSGj:matrix.r2.enst.fr) at `#mooc-floss:matrix.r2.enst.fr`. (English and <abbr title="Many of the project initiators and supporters for now are French" >French</abbr> are both fine.)

## Course

The course source is sitting in the `course` directory as flat files and it has the same structure as an exported archive.

To update the course, a merge request should be raised against the default branch and get merged.
Once the changes land in the default branch, the `publish-course` CI job is triggered and should do the following things: log in to the Open edX platform, create an archive and import it.

To publish the course, the `publish-course` job is using multiple Gitlab CI variables defined in Gitlab settings.

- `EDX_STUDIO_URL`: Open edX studio URL, for example: `https://studio.courses.opencraft.com`
- `EDX_LMS_URL`: Open edX LMS URL, for example: `https://courses.opencraft.com`
- `EDX_AUTH_CLIENT_ID`: Open edX OAuth2 application client ID (under `admin/oauth2_provider/application`)
- `EDX_AUTH_CLIENT_SECRET`: Open edX OAuth2 client secret
- `COURSE_ID`: Course ID , for example: `course-v1:MOOC-FLOSS+101+2021_1`

## Contribute

We welcome all contributors! You can find the details about contributing to mooc-floss by following our [contribution guide](CONTRIBUTING.md).
