#!/usr/bin/env bash
# This script:
#   - builds the marketing site with a given base path ($1)
#   - copies the site to a given folder ($2)
#   - compresses site assets with gzip and brotli
# Requirements:
#   - bash
#   - npm

set -euo pipefail

__base_path=${1:?"Please provide a base path for the app"}
__build_dir=${2:?"Please provide a build directory"}

__script_dir="$(dirname $0)"
__site_dir="$(realpath ${__script_dir}/../../marketing-site)"
__extensions_regex='.*.[htm|html|txt|text|js|css|svg|png|jpeg|jpg]$'

function log {
  echo -e "[$(date -u)]: $*"
}

if [ ! -d "./$__site_dir/node_modules" ]; then
  (cd $__site_dir && npm install)
fi

mkdir -p "$__build_dir" &>/dev/null

log "building site"
# set the environment for the base path configured in svelte.config.js
export VITE_BASE_PATH=$__base_path
(cd "$__site_dir" && npm run build)

log "copying site to $__build_dir"
cp -Rv $__site_dir/build/* "$__build_dir"

log "compressing assets with gzip..."
find "$__build_dir" -type f -regex "$__extensions_regex" -exec gzip -f -k {} \;

log "compressing assets with brotli..."
find "$__build_dir" -type f -regex "$__extensions_regex" -exec brotli -f -k {} \;
