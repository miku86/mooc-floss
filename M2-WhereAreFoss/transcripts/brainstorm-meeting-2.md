# Brainstorm - Module 2 - Meeting 2 Transcript

Video: https://digimedia1.r2.enst.fr/playback/presentation/2.0/playback.html?meetingId=73d3cd53ec8ede3f636b9a782ecc9f6375cf2271-1618247068140

S1: 00:00:00.852 I forget to start it, so, yes. Okay. So let's start on the second week work. So the goal of this work is to introduce people to Git/GitLab/GitHub workflow. So workflow where people have to basically fork a project, do some modification, make a merge request, and link to an issue, which is usually what most projects do nowadays. Whether it's on GitLab, GitHub, or other software forge. It's now the same pattern in Launchpad, I think. And I'm not that familiar with the one you use at OpenStack.

S2: 00:00:46.745 Gerrit?

S1: 00:00:48.206 Gerrit, yeah?

S2: 00:00:49.394 Yeah.

S1: 00:00:50.713 Okay.

S3: 00:00:54.223 Which one?

S1: 00:00:55.548 Gerrit.

S3: 00:00:57.453 Gerrit.

S1: 00:00:58.253 Gerrit.

S4: 00:00:58.712 Is it the same process exactly, or are there some steps--?

S2: 00:01:03.982 It's pretty close. Instead of Git push it's Git review, and then it goes up to a remote that exists, rather than having to set up your own local remote and fork from the original repository. It's basically the same thing happens, but it's a slightly different model where it's more of a push model than a pull model. But largely the same.

S4: 00:01:37.637 All right. So does that mean that there could be a description of a process that describes both GitHub, GitLab, and Gerrit that would make sense? Or will we need to be specific?

S2: 00:01:55.918 I think maybe Ildiko can help here a little bit better because I actually use the Git review command where she uses the Git push stuff and does it a little bit more aligned with the regular GitLab model, or GitHub GitLab model. But I don't know. Ildiko, do you want to?

S5: 00:02:19.914 Well, I mean, it's not necessarily that much more in line just because I type Git push as opposed to Git review because Git review does the same exact thing in the background. Well, I mean, good question. I still haven't tried how GitLab works, so I can't really say how it's different or not from that perspective. GitHub, well, Git push itself is kind of the same, but everything else is different.

S2: 00:02:58.485 I feel like a lot of the things are done under the covers in less steps than GitLab or GitHub that clone or fork and then you clone your fork of the repository, and then you push to there. And then you do the pull request or whatever. That middle area, the version of your repository in your account or whatever that's the fork, is what we push to for a code review, rather than having to do the extra step of the pull request. So I don't know. I feel like it's simpler, but lots of people aren't as familiar with it.

S4: 00:03:48.486 Yeah, we might. Sorry--

S5: 00:03:51.520 No, I just wanted to say that, yeah, Gerrit does some of this stuff in the background. And it really focuses more on the code review and the collaborative aspect of that, as opposed to everything else that GitHub does.

S2: 00:04:09.092 Yeah. There's a lot more ownership and more compartmentalization in the regular Git workflow, that GitLab and GitHub use where Gerrit is definitely more focused on the collaboration, like Ildiko was saying,

S5: 00:04:29.407 Yeah, and when you're using Gerrit, you're practically focusing on your commits and amending them. While with the GitHub, and I assume the GitLab workflow as well, you're just adding new commits into your branch in your fork and pull requests and stuff. And then at the end, they may squash or don't squash them, that's kind of, depending on the project and how their workflow at the end looks like. But it's also something that can throw people off if they are not used to. So if there's a comment on your commit in Gerrit, then you fix it in your local repository, and then amend your commit and upload a new version, and Gerrit will maintain a chain of changes based on a change ID. And this is completely different from how it's all handled in GitHub.

S4: 00:05:37.176 I see. So yeah, so it sounds like we'll probably have to do two different path at least because otherwise, that's probably going to be so generic that it's going to confuse people, at least from what I understood.

S2: 00:05:52.310 You still have to clone the repository, but what you actually clone from is different. And yeah.

S4: 00:06:00.389 But actually, I think it might be interesting. I know very little of the Gerrit workflow, but facing a course like that, I will be actually curious to know that the two ways of doing things because then that also opens to know it for my project. So that might be interesting.

S2: 00:06:23.170 Yeah, I have much less experience with GitHub and GitLab than I do with Gerrit working on a large scale. I've done things like individual repositories, but like large open-source ones with, many, many changes happening, I don't have a whole lot of experience there. I was trying to help one of our interns the other day do something, and I was like, "I legitimately have no idea how to do this." I was like, "I'm going to have to google it just like you are because I don't know how to fix merge conflicts this way. We'll figure it out," so yeah.

S5: 00:06:58.514 Yeah, the other thing, with GitHub, at least, is that they are supporting more and more things from the web UI. So in some cases or maybe many cases, you don't even have to clone anything to your local machine because you can just do it on the GUI. And it's not applicable for bigger code changes, for sure. At least, I would not want to do that. But I'm still wanting to bring it up because, for smaller changes and things like that, some people might prefer to just do that. And there's an option to.

S4: 00:07:40.335 Yeah, definitely. I use that often actually, on the [inaudible] course. Like sometimes I do it in my editor, but often, I do it from GitLab, actually directly.

S1: 00:07:51.809 Yeah, GitLab takes the same course of allowing to do everything in browser.

S5: 00:07:58.431 You can do some edits in Gerrit too, but you have to upload the change first. And then you can edit the review comments in your browser. But that's, at least, as far as I know, as far as it goes.

S1: 00:08:26.664 I've put in the chat the link to the Calc. So it would be nice to find some sort of structure for this course. Basically where the students will-- what the students will have to get during this week. So I've put four things which is the first one, what is software forge basically? So what's the common thing that people use to collaborate on code, and why they use it maybe? I did not put the why, but we can detail the sub-chapters. We will have to talk about workflows. We will want to get students who have some practical things done with GitLab as exercises or as-- we want people to do stuff like activities. And either a quiz or an activity for [the end?].

S2: 00:09:30.333 I would think the activity is getting them to push a patch or do a pull request or whatever to the-- I would think that we'd want to have some sort of sandbox repository for them to mess around with so they're not worried about breaking things or whatever.

S1: 00:09:50.982 Yeah, definitely. My initial idea during the brainstorming was to have a GitLab instance for the course. This GitLab instance would have LTI identification so that people would connect from Open edX into the GitLab. I'm pretty sure it can be done. I'm almost sure it can be done because GitLab has a quite generic identification module that can be linked to LTI, I think.

S2: 00:10:29.907 Okay. Yeah. That makes sense. That'd be easy to just have one--

S1: 00:10:34.775 Account.

S2: 00:10:35.791 Yeah, one account for everything.

S1: 00:10:41.116 And then from that, we can have toy repositories in that place for them to contribute, like just pull up a merge request that will [inaudible].

S3: 00:10:55.743 The protocol gives anonymous ID?

S1: 00:11:05.961 I'm not that familiar with LTI, but maybe Xavier is?

S3: 00:11:10.590 Yeah, so LTI is anonymous, so there is no way to get an email or anything. You just receive an anonymous ID, and you have to push back a grade with the same ID.

S4: 00:11:28.594 So yeah, but by default in Open edX currently, that's actually true. I mean, there are ways to go around that because we can always ask for those information from the LTI tool. Or there might also be a way around, I don't know, maybe potentially, SSO. But there's also another possibility, which is to implement some external forums like Discourse in Open edX. We're actually currently discussing with edX to add some exchange of information with LTI tools, something that might be like an opt-in per tool or something like that, but that might become possible. I don't know if that would actually be the case, especially for the beginning of this course, but it's important to know that this is being discussed, actually, at the moment.

S3: 00:12:27.905 It does exist on Coursera though, with Coursera plugins and their API.

S4: 00:12:34.751 Yeah. The capability is there. I think it has been mostly edX historically, who didn't want to pass on personal information to external tools. But, yeah, they are reconsidering that apparently, so.

S1: 00:12:53.420 But we don't really care if the identifiers are anonymous on the toy GitLab instance, right?

S3: 00:13:00.104 Yes. That's right.

S1: 00:13:03.220 As long as we can link a merge request done with an identifier for our students, then we can [grant?] it.

S4: 00:13:11.825 Yes. So you can definitely access that in the database. And the ID for each student is unique, so it's preserved over time. So it's definitely meant to allow to identify uniquely the users. But it's true that it's not generally the personal information. And I mean, in general we'll need to have some information about the student because, I don't know, if we want to check their contribution on their GitLab, GitHub, or another project and etc., we'll be asking them for their account names, this kind of thing. So we'll need to have that in some form.

S1: 00:14:05.779 Hello, Loic. I didn't see you before. You cannot see me because I cannot put my camera on. So sorry.

S6: 00:14:13.937 I was here the whole time.

S1: 00:14:23.101 About the week's schedule, the module schedule. What do you think about the current process? So week one we start by an activity, and this one we do not. So I don't know if we can change that? Could we have an activity that is not a software forge activity but would introduce people to the concept? Or should we just start by standard videos or text about it?

S2: 00:14:59.109 I think since it's going to be about them getting their Git setup, configured, and whatnot, we can start with text because it will be kind of like activity in parallel to that because they'll just set up all their local configs and stuff.

S1: 00:15:18.955 Oh yeah. We can do that. I completely forgot that you have to set up Git in order to use it.

S2: 00:15:25.582 Haven't had to do it in a while, so easily forgotten for sure.

S1: 00:15:29.672 Yeah. And even when I have new computer, the first time it tells me, "Oh, you have to do Git config something." "Oh yeah. Right," and then I forget about it.
[silence]

S2: 00:15:55.015 So I don't really know what you want to cover in the software forge part, but yeah, we'll need to do the Git setup, definitely.

S4: 00:16:08.767 And one thing to keep in mind is that one of the last things that we ask at the end of module one is to go say, "Hi." Maybe I get into contact with one of the projects. So if they do it over time, if they do one module at a time, the beginning of module two might be when they hopefully got an answer or got some interactions with Upstream. So we can always wait for the end of module two to follow up on that. But maybe there could be an interest in having something small at the beginning where we say, "Go check it. Say thanks if you have got something," or something related to what will be there this week. Try to find if they have a GitLab, GitHub place where the code is or something like that, and then we explain the rest. I haven't really thought that through, but I just wanted to mention that there is the outcome of the end of module one activity that might have evolved since the last time that they looked at that, and we might want to do something with that.

S2: 00:17:27.849 Yeah, you could make the last activity in this one to go and clone their repo and do a pull request or whatever too if that project has a equivalent of a sandbox throwaway repository to make sure that your Git setup is all correct.

S4: 00:17:53.972 Yep. Actually, you even have dedicated projects for those sandbox test pull requests. I've seen that being used. Basically, it's kind of nice. Actually, they come and they approve it and etc. So maybe that could be actually the activity [to not?]-- because I don't think most projects will have that sandbox except maybe the bigger one. So like providing one where we are allowed to do that, then that can be actually the sandbox GitLab that we put in place potentially. But it's also important to know that some projects exist for that, so you might not even have to redo it.

S2: 00:18:34.043 Yeah.

S1: 00:18:40.111 I didn't know that existed. Some--

S2: 00:18:44.090 Yeah. [crosstalk]--

S4: 00:18:44.987 Yeah, I'll find the link, yeah.
[silence]

S5: 00:19:01.025 The setup will also probably be different in the sense of using SSH key or not using SSH key and things like that. That can be different depending on the project and what and how they are using.

S3: 00:19:18.835 Do you mean that some projects are disabling--? Well, that doesn't impact anything if you clone and-- so yeah, yeah, I was thinking about HTTPS versus SSH access to the repository.

S5: 00:19:45.017 Well, I usually do SSH, but I did not use Github with that, and I think I was warned at least for one project, to set up the SSH keys and switch over to that. So that's what reminded me to bring this up. I don't know how all other projects are set up or if it's a GitHub level thing. I haven't checked honestly, I just set up the SSH key and then didn't care further than that.

S6: 00:20:13.697 I agree that SSH is better.

S3: 00:20:13.924 So it means that some projects-- sorry, Loic.

S6: 00:20:18.263 No, I mean, when the course was really in a short period, I tended to use HTTPS. But I've noticed too that in some places, HTTPS is either not working or forbidden or deprecated, etc. And you can imagine that it will not improve. So given the length of the course, it makes sense to go SSH only, I think.

S5: 00:20:51.561 Or at least warn people that it's probably a safer choice if available or something like that, just kind of preparing that there are multiple ways to do the same thing.

S1: 00:21:02.867 Is it easy to set up Git for SSH on Windows, for instance?

S3: 00:21:07.378 No.

S6: 00:21:10.028 No that's why I was mentioning it because most of the students had Windows, and it took them about one hour to download, figure out things, stumble on issues, etc.

S3: 00:21:26.236 And it depends on the version of Windows you have. Like on Windows 8 or Vista or something like-- all the old versions of Windows, you have extra steps that are quite cumbersome. And on the latest Windows 10-- because on the first version of Windows 10, like at least one year old, you have to install manually Git, etc. But on the latest version of Windows, you can install Git directly within the parameters of Windows. So you go to somewhere and you just check a box somewhere, and it installs OpenSSH on Windows automatically. You don't have to download anything. It really depends on the version of Windows you have.

S2: 00:22:24.913 At the end of the first week, we could have that be homework for the second week to make sure that everybody gets Git downloaded, or installed rather. Because then they'd have however much time in between, whether it be a weekend or obviously they're kind of self-pacing through this whole thing. So if that takes them a while, they could have that ready, and then everybody would be roughly on the same page when they start week two, if you give that as a prerequisite for the second week.

S3: 00:23:00.928 Yeah, I prefer to talk about modules rather than weeks. Like you said self-paced, it's exactly that. We assume that one module could be done in one to three weeks, we don't know.

S2: 00:23:20.192 Right. Yeah. So yeah, I guess maybe at the beginning of week two, you set that as a prerequisite then be like, "Before you actually go through this material, make sure you have Git installed, whatever that looks like for you and your system."

S1: 00:23:40.168 Definitely, that could be the first item on the module two.

S3: 00:23:44.096 Yeah, that's what you've written, Marc.

S1: 00:23:49.009 Feel free to edit the Calc. I just put stuff, but.

S3: 00:23:52.238 Locally set up Git. That's an activity that you put at the beginning of week two let's say.

S2: 00:24:02.290 Yeah. Maybe include install and setup. Or have installation first, and then you can do the software forge stuff. And then configure Git or whatever.

S3: 00:24:22.687 So I think the best would be to just point to external resources for Git installation and setup. Because there are so many different ways of doing that with Mac, with Windows, depending on Windows version, Linux, depending on distribution.

S6: 00:24:37.731 I agree. And they are very well done too. And very up-to-date. So very well curated.

S2: 00:24:45.733 Yeah. Rather than duplicating the information again, and having to [implement it?] ourselves.

S4: 00:24:53.841 Yep, and if they see something to improve again that gives an opportunity for contribution, which might be a good thing to just repeat through the course whenever we can give an external resource to say, "Hey, by the way--"

S6: 00:25:06.973 Unfortunately, the best-curated resources regarding that are on GitHub and GitLab, and there is no way you can contribute. So that's a nice idea, but that--

S4: 00:25:23.421 Why there is no way you can contribute? You mean all of the documentations you can't contribute?

S6: 00:25:27.967 No, not all. But in this case, you want to point the students, the participants to a resource where they have the maximal chance of success because it's just a hoop that you want to jump over. And these are GitHub or GitLab, and they do not allow you to contribute there.

S1: 00:25:51.063 Well, GitLab does.

S4: 00:25:53.094 Yeah. With GitHub, I'm not completely surprised because they are proprietary software. But, yeah, GitLab they have everything open now.

S6: 00:26:01.780 Oh, yes. Of course. And they don't do any proprietary software either.

S1: 00:26:06.734 This [may be?] true.

S4: 00:26:09.214 They do but a lot less. And they are [inaudible]. Comparing Microsoft to GitLab is really apple to oranges. Even if it's still not peaches, I guess, yet, but.

S6: 00:26:20.415 No, last time I checked they didn't allow people to contribute. But if they do, well fine. My point was just the priority here is to get the people to use SSH.

S4: 00:26:35.131 Sure. Sure. Sure. But it was also about considering the maintenance on a long-term if there will inevitably be a time things that are not completely up to date and stuff. Yeah. That's a way to take off some load from us besides just pointing to external resources but also, when applicable of course, to tell the students they can update it if it's not up to date.

S3: 00:27:01.219 And regarding the external resources on like a procedure or set of instruction to install something. I many times went through GitHub gists-- or gist, I don't know how to pronounce that. So I put this in the chat. And the equivalent on GitLab is called code snippets. And sometimes you have Git tutorials with a few steps just put in a gist. I don't know how to pronounce. And many times when I search something, either I find it on Stack Overflow or on a GitHub gist.

S4: 00:27:53.772 Yeah, I don't think you can do pull request or merge request on those, but they are Git repositories. So, yeah. At least the gist. I think we say gist, but I'm not sure either.

S2: 00:28:07.503 I pronounce it, gist, but who knows?

S3: 00:28:13.702 So, Loic, you've put the official documentation on use SSH on Windows. So they say, "If you are running Windows 10, you can use Windows Subsystem for Linux." But again, it really depends on the version of Windows 10 that you have. Old version of Windows 10 have WSL 1, and new ones have WSL 2. And they are very different approaches. And also when you set up everything Git with WSL, then, oh, it's a mess when you want to access local files with WSL 2. It's really difficult. So I would not recommend to use WSL to install Git on Windows. I would rather recommend the official way on the optional components of Windows, and then you check the box, "OpenSSH," and everything is configured correctly. Or go directly to the official Git website and install Git for Windows, and it will set up everything correctly also. So those two ways are the best. I don't know why they recommend WSL. I wouldn't do that, but.

S2: 00:29:43.706 Sounds like you have a lot of knowledge here and could write the section of the--

S3: 00:29:48.594 Yeah. So Git and Windows because I'm responsible in my school of the GitLab and the Git course for the first year, so. And then all the students come with different systems: Linux, Windows, Mac. And I can see all the different situations. So yeah, I'm kind of aware what's happening.

S2: 00:30:15.041 That's really, really helpful [inaudible]--

S3: 00:30:17.039 So I could write some things and point to the correct-- I mean, to the good resources because it changes a lot.

S2: 00:30:28.065 Yup. Sounds like you should volunteer to write this section. [laughter]

S1: 00:30:34.949 Remi, I volunteered you for the section, if you look at the column of the person in charge.

S3: 00:30:39.779 Sure. Yes. I mean, especially for Windows. But if someone wants to do that for Mac or Linux, that could help. I mean, find resources, good resources how to install Git for Linux or Mac. How does it work on Mac, actually?

S1: 00:31:04.353 It's a Unix, so.

S3: 00:31:06.439 Yeah, but how would you install Git? Is it already installed, or do you have to install it with official Apple thing or Brew? Yeah, but then you have to install Brew also.

S5: 00:31:24.368 I honestly don't remember how I set it up.

S3: 00:31:26.876 [crosstalk] because once you've done that-- you do it once, and it updates automatically I guess.

S5: 00:31:33.745 Yeah, I guess. I don't really care about it too much and it still works. The only thing that I had a little trouble with is Bash completion, so you can use tab to complete the commands. That took me a good 5 minutes of googling to figure it out, or maybe 10. But other than that, it really wasn't complicated at all.

S1: 00:32:03.137 Git was in Xcode and that's probably what many people use on Mac.

S5: 00:32:13.261 Now that I don't use, so I have no idea how to connect the two.

S3: 00:32:18.257 So even on Mac, you have different ways of installing Git.

S5: 00:32:23.207 I only use the command line, so I don't use-- I write stuff in Xcode, but I don't have it connected to Git or any other version control. I just use the command line tool and, yeah, the commands and that's it. I'm old-fashioned.

S3: 00:32:46.327 So here, we will need a lot of mentoring, Xavier.

S1: 00:32:56.015 So what should we ask people to do on a toy GitLab?

S5: 00:33:03.948 Also, just one thing. I think we should focus on the basics that get things done for them because if they are seasoned developers, they probably have their preference already. If they are not, then they will probably build one for themselves over time. So I would not necessarily go into a lot of details on how to set up complicated scenarios, when people may or may not want to go that route.

S3: 00:33:33.776 Yeah. Yeah, so that's why you have put, I think, the push model, pull model. We should just do a simple activity on-- how do you do-- what's the difference between push model and pull model again? [laughter]

S2: 00:33:55.502 So it's basically, the pull model is setting up a pull request. Where the push model is like how we do things with Gerrit, basically, where you don't have to have the forked repository in the middle that you are using as the equivalent of a remote location that we have. And so it's like you're pushing this up to a place rather than pushing it up and then doing the pull over into the main repository. Yeah.

S3: 00:34:32.606 Okay. Okay. I have to do it myself to understand. So the push model is specific to Gerrit, right?

S2: 00:34:46.854 Yeah. Basically. I can send you our documentation. [laughter]

S4: 00:35:00.302 And I think for your question, Marc, for what to do, we could probably base it on the description that is on that first contribution project that I was mentioning which is for GitHub. But we could do a GitLab version of it. And its MIT license. So we could just use that as a base and rework it to make it like a GitLab. And I mean, we could also search if there is an equivalent on GitLab already because then we wouldn't have to manage that at all. But I think that contribution, that workflow of creating a pull request like cloning the repository, making a change from pushing, getting a pull request open, getting that merged, I think that's the main thing people have to have to learn before doing a contribution and doing it live on anyone's project. That's probably a good thing to have done once.

S3: 00:36:02.551 Ah, so indeed the processes fork, clone, branch, commit, push, and create the pull request.

S4: 00:36:18.172 Yes.

S2: 00:36:19.150 Right.

S3: 00:36:20.804 Would it be the same on GitLab? I guess, yes. Fork, clone--

S1: 00:36:24.421 Yes.

S3: 00:36:26.430 But the last one is to create a merge instead of a pull request.

S1: 00:36:32.894 Yeah, but that's the same. The pull request creates a merge commit. So I think I prefer the GitLab terminology.

S3: 00:36:41.227 Okay.

S2: 00:36:43.144 Yeah. Gerrit, you just clone, branch, commit, get review.

S5: 00:36:53.817 We just practically push.

S2: 00:36:55.713 So push, get review.

S5: 00:36:56.802 But that's the push model there. [laughter]

S2: 00:37:00.044 But that's it. There's no extra steps while making the merge request, pull request thing.

S3: 00:37:04.869 All right. All right. I see.

S5: 00:37:06.960 And also, you don't need to do the fork either. And even with the branching, it's practically for your own sanity and safety locally because you're practically doing everything in your local repo. So if you screw something up so badly, you delete the whole thing and clone it again. You're fine. So easy. [laughter]

S1: 00:37:31.308 It's similar on GitLab. You can technically work on the master branch on your fork if you want. But it's usually a very bad idea because it makes things more difficult for you.

S2: 00:37:42.952 Yeah. When you've got to clean up merge conflicts later, it is an ugly, ugly mess.

S1: 00:37:48.900 And also sometimes it's protected branch on the repo configuration, and so you have more difficulties working with that. You cannot rebase it because it would modify the history, which you can do on your branch, but you cannot do on master if it's protected.

S5: 00:38:13.714 Yeah, and Gerrit just keeps everything on a review branch or something in the background, and you don't have to deal with lots of that stuff.

S2: 00:38:22.405 It is so simple. So simple.

S3: 00:38:28.661 What's interesting on the link you sent, Xavier, the first contributions on GitHub, is that they have tutorials for GitHub Desktop, Visual Studio, GitKraken, Visual Studio Code, Atlassian Sourcetree, and IntelliJ IDEA. And so [crosstalk]--

S4: 00:38:57.096 Yeah, that's nice actually. I didn't see that.

S3: 00:38:59.330 Yeah, because the thing is-- so the one you sent is on the command line, and I can see it uses only Git, so that should be okay anyway. But still, the prerequisite here is that when you Git clone, well you have to open a terminal, go to the correct location, and do the Git clone there. And even basics with the terminal-- so we mentioned they are prerequisite for the MOOC, right? So do we want to make sure they have at least the prerequisite to go to the correct folder, and?

S4: 00:39:51.206 I think last time we discussed that we said that, yeah, maybe some people might not be completely compatible with that when they arrive, but that there are also some pretty good resources online today so that we might direct them to the right place if they don't have that prerequisite. Then they can learn it and then come back. So that might be something to remind here in case they skipped some of those earlier sections, just a quick reminder. Yeah, if you're not sure what to do there. But I know from first-hand experience, second-hand experience, I've seen someone who was starting with contributions and with command line and etc. with just a little bit of command line knowledge before. So just a very quick lesson and nothing really advanced. And this tutorial was actually really easy to follow and felt like a good experience, generally like feels very smooth. So probably doesn't need a lot. I don't think we need to point them to something very advanced. But having just a little bit of the base command line like cds and etc., that should be fine.

S3: 00:40:59.536 Okay. The thing is when-- so do we want them to go to see something in the log like Git log, or it's only a matter of commit push, pull request, merge request, and that's it, or?

S1: 00:41:19.175 Maybe later.

S3: 00:41:20.588 Maybe later.

S1: 00:41:20.885 Here we will talk about the contribution workflow. And on week five or six - I don't remember which one - it will be useful for navigating in codebases and find stuff in history.

S3: 00:41:36.541 Okay, I see.

S1: 00:41:40.298 But that's two quite different things that Git can provide that it can allow you to contribute with others, and it can allow you to dive into a codebase basically and see what has been done and why.

S3: 00:41:58.323 All right. So but anyway I went through the tutorial with Visual Studio Code, and it's pretty great. It doesn't use any terminal at all. So that's another option.
[silence]

S3: 00:42:32.768 All right. So what else do we want to have in this week?

S2: 00:42:42.807 I feel like content wise getting your Git environment installed and set up and then doing a first contribution to some sandbox repository that we set up in GitLab is a lot. It doesn't sound like a lot because all of us do it regularly and are really familiar with it. But I know when Ildiko and I run Upstream Institute for OpenStack, this part takes a long time because there are inevitably an issue with somebody's system, and they can't set up an SSH key, or they don't have permission and there's a firewall. And yeah, you're familiar with the number of pitfalls [inaudible] typically, so.

S5: 00:43:32.339 Yeah. We even do have a prebuilt virtual machine that most of them are using that has a lot of stuff preinstalled, and even that way we have a lot of trouble.

S2: 00:43:45.071 Yeah. We do that to try to get everybody roughly at the same point. And everybody's working with the same virtual machine. And we control what's installed on that virtual machine or the virtual machine image or whatever. There's a little bit more consistency, but they're inevitably like, "Oh, this person only has four gigs of RAM. Well, how do you do life? I don't understand." Yeah, so I think that this is probably plenty of content for this week.

S3: 00:44:16.565 Right. Yes.

S4: 00:44:18.822 That actually reminds me of something we had mentioned during the first brainstorming meeting, which was to use things like Gitpods and etc., which might be even better than the container images or the VMs because that can actually be set up on the server side maybe with already the rights that correspond to the account that clones it and etc.

S3: 00:44:46.509 That's a very good idea. So can we have GitLab with GitHub-- oh my gosh, GitLab with Gitpod IDE? Marc?

S1: 00:45:05.091 I don't know. I think it was not open source.

S3: 00:45:11.022 Yeah, so Gitpod is based on Eclipse Che or Theia IDE, I don't remember, which is open source.

S1: 00:45:22.568 But you mean to have an activity with an IDE, like a web IDE?

S3: 00:45:27.298 Yes. So the thing is-- so you go to this GitLab instance, and you can do everything for--

S1: 00:45:36.722 But we can use GitLab IDE then maybe? I don't know if it's in the free version or not. I will have to check that.

S3: 00:45:49.427 That's another option. It's lighter than--

S4: 00:45:56.317 I mean it's true that even the IDE might be something where you even remove Git altogether in that case. You're just doing the workflow of creating a pull request. And it's true that maybe before even adding the complexity of setting up Git and having to use the tool and command line and etc., already knowing what's the flow of like cloning branches. You don't need Git for that. And maybe the web IDE would be enough.

S3: 00:46:28.556 Yes. I agree. I totally agree actually.

S1: 00:46:31.035 You can use GitLab IDE.

S3: 00:46:34.476 Cannot or can?

S1: 00:46:35.435 We can. There is no problem. It's in the free version.

S3: 00:46:39.592 Ah. All right. And what about Gitpod?

S1: 00:46:47.991 GitLab. Gitpod is not open source.

S6: 00:46:51.844 The problem I have with the GitLab web IDE is I've seen people using it and they were not familiar with contributions, and something they cannot manage to do apparently is to rework, that is to address a review on a commit. I never figured out how to explain to them how to do it properly. They end up having multiple merge requests, having a series of commit that make no sense with no comments. They are lost in the branches etc. So in this case, it seems like it's more suited for a one-shot contribution. But it sounds like an Upstream course needs to address how you rework, how you address a review. And there, I didn't find a use case that actually works with the GitLab IDE. Maybe there is--

S4: 00:48:00.355 I did.

S6: 00:48:02.005 Sorry. What?

S4: 00:48:02.473 Actually, I did because in OpenCraft, we use GitLab for pretty much all the editing of documentation, the handbook, and etc. And people who are not developers in the team have to do that too. So definitely the first contact with it, I've had some cases which are a bit what you described, to make a change, they don't understand the open a new pull request and etc. But once you show them and you explain a bit more, actually I've had completely non-technical users who routinely use GitLab to make pull requests, to address comments and etc. And that seemed to work well. It's really important to have the right conversations in the beginning. So I'm not sure exactly what works and what doesn't. We might have to do some trial and error because it was also very maybe one-to-one mentoring in those cases, but it does work. At some point, something clicks and they are, "Ah. Okay. That's it." And then they use it.

S1: 00:49:04.669 I have the same experience with Inkscape non-code contributors who also use GitLab.
[silence]

S2: 00:49:20.053 While it's nice to remove as many of the barriers as possible, I feel like we definitely need to make them do the struggle of setting up Git at some point [laughter] because it's not realistic to live in a lovely bubble of IDEs in which you don't touch a terminal because that's not how open source works. [laughter]

S1: 00:49:42.514 Yeah. I'm sure. Also--

S2: 00:49:44.134 I totally understand easing them into it though, and teaching them the mindset and the flow of how things work before getting into the nitty gritty.

S1: 00:49:54.736 Also, there's sometimes you just have to code locally, because here my CI takes one hour to run. And I can locally compile in 15 seconds because I only compile the files that I changed. So it's not practical for me to code in a web IDE. It's just not possible.

S2: 00:50:20.307 [crosstalk]--

S6: 00:50:20.647 I agree, but-- sorry, go ahead.

S2: 00:50:24.541 Oh. Go ahead. It's okay. I was just going to say that usually people don't always have Wi-Fi either. You might be on a plane or whatever, and you might not have Wi-Fi. So having it cloned locally to be able to work on things is also a another reason.

S3: 00:50:39.104 And the philosophy of Git is to have multiple repositories locally. The original philosophy of Git.

S6: 00:50:48.411 I fully agree and I could-- but, so the very nice thing about using the web IDE as a first step, is that the participants can focus on the concepts, and not being encumbered by the tools. And when they learn the tools, they can focus on the tools and not learning the concepts. So that's a very nice first stepping stone. I did not know that you could rework a commit within the web IDE of GitLab because this is the conceptually bizarre thing that participants struggle with at the beginning. Is they make a contribution. They make a change. But the very idea of changing a change is bizarre. And if they are in a web environment with no tooling to worry about, then they can focus on this bizarre concept that they need to assimilate.

S4: 00:51:51.671 Just to be clear, you can't really edit a commit. You would add more commits to your branch, and then they can be squashed automatically at the end if that what the project requires. But I don't think you can do a Git amend from the web IDE.

S1: 00:52:07.234 I think you can.

S4: 00:52:09.765 Oh? Then I don't know. Okay.

S6: 00:52:11.539 Because this is the one concept that is super difficult for first-time participants to grasp. Okay, you want me to change the changes that I made, but let's redo it again.

S4: 00:52:30.470 But I think it might also be the difficulty here because you have different steps in that. The change they might see it as the pull request at first. So adding a new change to that pull request is a first step, I think. And then, if you want them to amend their commits and etc., then I think we step into the world of you need actually a Git installation etc. So that can be a second step. Because, generally, in a lot of cases you don't need to do actually a Git amend. You can just have a series of commit and squash them at the end.

S6: 00:53:08.059 Okay. But they would need to experiment the squash idea because the idea that is bizarre and difficult to grasp is this squashing, rebasing, amending. And so even if it's at the end of the merge request, but they see there all their small changes being squashed together, and they can visualize it. And it helps. Say, "Oh. okay. This series of change that I made, it's actually one nice, tiny change."

S2: 00:53:45.008 So even that is different from project to project. So I don't think we want to get to far into the weeds with that because with OpenStack you have your local branch on your computer. You make whatever changes. After you've done the initial push, somebody made a comment, and you're like, "Oh, I got to go fix some things." So you fix it locally. You Git commit --amend and then you Git review again. And it just pushes up the next patch set. You don't have to worry about squashing or anything like that.

S5: 00:54:14.875 Well, but you have to amend. So if someone is not used to the amend part, then it can be challenging. But how Gerrit is setup, it's really important that you do amend. Otherwise, it will become a big mess.

S6: 00:54:32.344 And even Gerrit has the notion of patch series also. It came in very late. There are relationships between commits, between reviews. I don't remember the specifics, but it's very advanced. So you don't have this notion of a series of commits that you usually, commonly have on GitLab or GitHub. But still you have this idea that the change tells a story. And in Gerrit the story is told differently in the sense that the story of where you want to go, patch after patch, is told by a series of reviews of individual commits. Where in GitLab, there is one merge request, and you have a series of commits within this merge request that explain the steps that you went through. But this is even more advanced because it requires first the student to master the idea that a commit is a tiny story to be told and to be amended like a draft on a paper, and yeah. Sorry, I get carried away.

S2: 00:55:46.753 No. [crosstalk]--

S3: 00:55:47.971 Maybe do we want to make them an activity that focus-- I mean, it's quite advanced but on the difference between merging versus rebasing.

S4: 00:56:05.617 I mean, the thing is that Git, you can go pretty far and have a lot of things. So the question is do we want to redo a whole sub-course about Gits. That might be where we want to stop. And, I guess, on all those points it depends how progressive we want to be. Because there was also another thing to get familiar with Git concept, that we had talked before which were the games, where you basically do a series of steps. You are asked to rebase a branch, squash commits, amend and etc. So that might be a way to give something nice for this without us having to redevelop the whole course content. And for people who want to go really further, we can always link to a Git course or a Git manual or tutorial somewhere.

S3: 00:57:02.507 Right. Okay. Yes.

S1: 00:57:06.796 Yeah. I agree that focus of this course should be on contribution and not too far into specifics of what they can encounter but will likely not encounter in Git. We can have an exercise based on, oh, you have detached head and you're now stuck with a detached head. But that's something you will encounter if you do just two commands in Git that you will find on the internet. But it's easy to get out, but getting on the difficult parts of rebasing or stuff like that is, I think, a bit too much.

S2: 00:57:48.883 Yeah. I feel like those are all things that people can learn in Git. And all the complexities and whatever, is removed from learning how to contribute to open source. It's a part of it, yes, and you need that knowledge, but that's not the focus.

S3: 00:58:09.972 Even the very basic concept of branch could be difficult. And that's almost a prerequisite, so. And even if I go to the very basics of versioning. We start the week with imagine you don't know anything about versioning, forge, etc. Well, you have to go from the very beginning, so.

S5: 00:58:44.347 Yeah. But that should not be part of this course, I believe?

S3: 00:58:48.428 No.

S5: 00:58:49.086 I mean, we are teaching people to work upstream and we are not teaching version control and all those things. There are prerequisites. Most people will meet these things during their work or already at university, I would imagine these days. So I don't think that it should be our responsibility to do so. And also, I think, Stack Overflow is pretty good at solving these things, so I would not want to steal their credit. People can just go and find their most favorable answer out there that works for them and be happy that they solved it for themselves or by themselves.

S4: 00:59:31.535 Yeah, especially, because that's a skill they will need all along anyway. We are definitely not going to write the code of their contributions for them at the end, so. I think maybe one middle ground we could have here is whenever we don't handle a subject, we say, "This is a prerequisite," or "Figure it out on your own." We could at least try to find one good resource that walks through people very well, so that we don't leave them hanging either. But we don't take the responsibility for that.

S3: 01:00:00.770 Yes.

S2: 01:00:01.146 Yeah. I think that sounds really good. Yeah.

S5: 01:00:04.512 I also wonder if we could come up with an easier question and give that as an exercise or something to find an answer to, as opposed to trying to find the best asset today. And then three weeks later, there will be something much better out there anyway.

S3: 01:00:19.035 And we will change it because it's a dynamic course, so.

S4: 01:00:23.054 Exactly. That's another thing. If you find a better resource for this, just create a pull request.

S2: 01:00:28.849 Exactly. Have the students do it. Ildiko and I do that all the time. We're like, "Here, go fix our mistakes, please. Thank you." [laughter]

S5: 01:00:36.023 Yeah. I just figure that maybe we can be a bit more explicit about that. Because when someone is overwhelmed when trying to learn all this, then they will not necessarily think about fixing the course material, at least not as a first step. But if you give them an exercise that this is a conundrum that you might run into. So go out and find the way to solve it, or something like-- I don't know. Just so that they kind of learn that they will not know everything by heart and from the top of their heads. And we are all using our favorite search engine to find answers anyhow. So they should be-- they should feel comfortable doing that too.

S2: 01:01:20.487 Great.

S4: 01:01:21.068 Yeah. Some very good points.

S3: 01:01:23.322 Yeah. I agree. Indeed, we should focus on - and, Xavier, you like it a lot - on designing activities, all the way through the MOOC rather than designing flat resources explaining theory. Yeah.

S1: 01:01:48.624 Polyedre, you have not spoken for a long time. What do you think about this?

S7: 01:01:54.922 I was thinking that-- I don't remember when I was struggling with Git. So as a student, I cannot explain what was difficult for me. And now I think that I'm quite fluent with Git. So I think the most difficult part for me was rebase versus merge parts. So maybe it is not the best place in the second module to explain.

S3: 01:02:30.789 Yeah. We shouldn't do that.

S7: 01:02:35.226 But if it is well described and we have the comments or the explanation of what a students needs to do, and why it needs to do these steps and what it does under the hood, or how it does it, or maybe it could be something good to do.

S3: 01:03:07.760 Did you have a specific course on Git?

S7: 01:03:12.046 No. No.

S3: 01:03:18.810 Even when I design Git courses at my school, I have so much difficulty. That's magical for you, so.

S7: 01:03:30.467 At first, it was just adding a repo on GitHub and not contributing but just pushing commits to save as a history. So there was no collaboration, or if there was collaboration in a project, everyone was pushing. And if there was an error, you just had to pull the commits and then push out change. So that was the workflow.

S1: 01:04:05.406 And then you add minus minus [force?], if you want to be right.

S3: 01:04:13.664 That's not what you want to teach. Not this procedure.

S7: 01:04:14.115 And [crosstalk] more recently that I tried and spend time understanding the fork merge requests workflow.
[silence]

S3: 01:04:39.025 Do we want to-- I mean, it's already time passed 17 minutes. I don't know. Do we want to put persons in charge for some of the activities we have in the table?

S4: 01:04:52.394 Is it? It's not one hour up. [crosstalk].

S1: 01:04:55.157 Yeah. We have 15 minutes left.

S3: 01:04:57.141 Oh, okay. Okay. [foreign].

S1: 01:05:05.515 Yep. So do you have ideas for activities that could be graded? This is a course, so we have to give grades, and we have to have special activities for people who want a certificate or stuff like that. So we have to find out what people could do that could be checked if they did it right or wrong. And so the activity that's not graded but part of this module was fork, clone, commit, push merge requests. We could ask a variation of that, like, do a specific pull request or merge request with specific contents or changes. Like fix a typo in this toy repo and make a merge request that does that and only that with a good title. Or we could have a peer reviewed activity.

S3: 01:06:07.333 Oh, no, no, no. Not here.

S1: 01:06:08.601 No? Okay, not here.

S3: 01:06:10.188 I don't think so. Not yet.

S4: 01:06:14.244 And, I mean, even the first ones, you said they are not graded, but why not, actually? Because if it's something where people can try as much as they want until they actually get to the result, it doesn't matter too much that it's one of the first times they do it. They just need to be able to show that they actually managed to do that to get the full grade.
[silence]

S2: 01:06:47.833 You could just have them make a file, and, I don't know, you could ask a question and be like, "Upload a file in which you answer the question, 'What do you love about open source?'" or something. Whatever. And then the fact that they can make a file and commit it and push it up is a good display that they have a reasonable grasp on how to do things going forward.

S1: 01:07:21.883 Or just add a line that says that for you in a file that's already in a repo, so that you will have conflicts, and.

S2: 01:07:30.626 Oh, yeah. Yeah.

S4: 01:07:35.483 And I don't know if that's a graded activity. But we also have to think, again, about the things we ask at the end of each module, which is to move a bit forward with getting to know the project, where they are going to contribute and etc. And here because we are talking about merge request and etc., it could probably be a good moment to get them to dig deeper into the history of merge request. Maybe that could be where they do their first, I don't know, typo merge request somewhere or improving the documentation in an area. Something like that, something that doesn't require code or at least very marginally, but puts into practice what has been learned this week in terms of the flow of merge request and Git and etc.

S2: 01:08:27.242 So you want them to do that in whatever project they're interested in?

S4: 01:08:32.581 Yeah. Because the first week, they would have, basically, gone to the forum or whatever place where people discuss, said hi, maybe say, "I'm interested in contributions," and etc. And generally, what comes really well after that is to do one pull request and merge on something really basic just to get, again, the flow of how to contribute. And that kind of fits well with what we are doing here where we are not really talking about code but the whole process of merge request.

S2: 01:09:03.463 Yeah. Makes sense.

S4: 01:09:09.584 And I guess, if you are looking for grading, so that would probably have to be more of a peer review thing, I don't know, but at least showing the merge request that you've done, the communication you've done around that, and getting other people to grade you on that. Maybe one of the advantage beyond the grade is that it would force people to really think about those criterias like, "How do I evaluate someone else's contribution? What do other people do? What kind of reaction do they get versus mine?" That might be interesting by itself, actually.

S3: 01:09:53.966 So the original idea we had with this personalized GitHub on our servers was to do automatic grading. So indeed, Polyedre, you mentioned in the chat that we could check automatically the contributions, like adding a name, [inaudible] a file, respecting guidelines in the personalized CI. We could implement that in Git many times. I don't know what to think about peer grading at this stage of the MOOC. Maybe it's good because you have good arguments, Xavier. Maybe it's a little bit too much because it takes a lot of time. Peer grading, as a student, you have to submit your answer and review three answers to get your grade, so it takes quite a lot of time.

S4: 01:10:59.747 Well, it takes a lot of time if you have to write down things a lot, right? Here, that could be mainly just giving the links to things you've already done in the activities. So yes, the thing that would take more time is to look at what other people have done, but that can also be-- I don't know. It depends if other people are doing shit or what, but it could be interesting to look at that. I don't know.

S3: 01:11:27.364 I would prefer, at the beginning, to have something fast and automatically grading, without peer grading. But still, I have to think about it.

S4: 01:11:38.462 I mean, if we can find something automatic for that, why not? But strictly, because it is-- I mean, having a merge request somewhere. And that is something we could eventually automate because you can enter the URL. And if we know the user name. But that might depend heavily on the project itself. If they're on GitLab, GitHub, that might be okay. But if they're on a random repository somewhere, yeah, maybe we have to think about that.

S3: 01:12:17.311 Maybe when they have chosen projects later on in the MOOC, and they are starting something here, maybe we could start peer grading, when we don't have control about what they do in external repositories. At the beginning--

S4: 01:12:32.734 Oh, but that's very much the-- that's very much the case already, right? Because basically, the first week, the idea is that they choose a project on which they start to talk with people. And maybe they might change it if they realize that it's not the right thing. So that's also why we start early. But at least that's the way I approach those end-of-module activities is that you're actually already working towards your contribution. At the end, you've picked your project. You're just starting to talk with them to make sure that you get all the information and context that you need by week three or four, I don't remember, when we actually want them to push a real patch. But that's how I see it. So that's why I mentioned that also at this point is that they are already outside of our fenced garden in a way because they're on a real project.

S2: 01:13:26.896 Well, two things, I guess. One, if we wanted, we could have them-- this pull request, merge request thing that they're doing here. If we like own this repository and don't really care what's in it, we could have them basically create a file that has all of the information about their progress or whatever. So they could say like, "This is the community I'm joining." And then they can be like, "Here is the email or link to the email to the mailing list that I sent to say hello and thank you," or like, "Here's a link to the logs that shows that I did the thing." And then they can continually add to edit that as we go through the course. And then other people can peer-review it, and we can see the progress that they're making and stuff like that.

S3: 01:14:24.695 That's interesting. The personalized progress book.

S2: 01:14:33.460 Yeah, because then other people can see what they're working on and maybe work together or not. Or at the very least, we have a little bit of insight into where they're at with things. And if they get stuck, we have that background information that they've been collecting and working on. And then it also doubles as a thing that they continue to work on, a file that they continue to make changes to, and will be merged, I guess, and merged and merged as they make changes to it.

S1: 01:15:06.916 Oh, that's a nice idea.

S3: 01:15:09.151 Yes, yes, I like it a lot.

S2: 01:15:12.399 You're welcome. I'm quite proud of that.

S4: 01:15:16.407 It's nice because it does allow you to do something progressively which is kind of the rhythm that we want for the activity at the end. And even for the peer grading if we want to have that then we don't need to do it necessarily do week where they do that. It might be on the third week or something. After they have complied enough then they have kind of a stop point where they look at what each other have done, and it might give them also a good standpoint to say, "Okay, this is the right project. This is the right approach," or, "No, I need to change." So, yeah, that's a great idea.

S2: 01:15:50.650 Yeah. They can go back and change if they do end up switching communities or whatever. Yeah. Cool. The other thing I wanted to mention was with our last minute here. Ildiko and I both are going to be in an event next week. Our project team gathering for OpenStack and all of the other open infrastructure projects is next week. And then the following week, she and I, having both worked hilarious amount of hours next week during this event, are planning on taking Monday off. So that'd be the next two Mondays, she and I will be busy/not around. So I don't know how you want to handle that, if you want to meet without us or cancel them or whatever, however.

S3: 01:16:48.759 So next Monday is who? Organization licenses, economic models, charities, blah, blah, and [inaudible].

S2: 01:16:56.258 Oh, well, I don't think I have a whole lot to offer on that.

S3: 01:16:59.665 And the next is community management, IRC, diversity designers or other types of contributors, history, get in touch with the community of the project, the bug tracker, everything.

S2: 01:17:18.435 Yeah, that one definitely, I have value I can bring to the table.

S3: 01:17:23.664 Yes, I think so.

S2: 01:17:23.943 But next week less so, I guess. So I don't know if you want to skip one week or whatever, or? Yeah.

S4: 01:17:32.646 So maybe what we could do is the one where you don't care too much, we could have it normally, and maybe the week after, we could have kind of a project management call, the same way we did last week because that was quite useful to review progress?

S3: 01:17:44.695 Yeah. Think so. Great.

S2: 01:17:46.832 Oh, yeah. Yeah, that'd be great. That would totally work.

S1: 01:17:51.173 A progress report week.

S2: 01:17:54.143 Yeah. A little bit of a bye week where not everybody gets more stuff thrown on their plate, and you can kind of do a check-in on how things are going. That sounds like a really good plan.

S4: 01:18:04.157 The only thing is that then you escape the project follow-up. So if you could, I don't know, maybe before you get into [inaudible] for that send a little update that we would use during that meeting, then that could be useful.

S2: 01:18:18.092 Yeah, I can do that for sure.

S1: 01:18:19.622 Cool.

S4: 01:18:19.622 Cool.

S1: 01:18:23.992 Do we assign people to this week? Where is [inaudible].

S3: 01:18:30.532 I always [crosstalk]--

S4: 01:18:31.055 Sure. I mean I--

S3: 01:18:33.294 Yes?

S4: 01:18:35.814 Sorry, Remi. What were you saying?

S3: 01:18:38.261 No, no. I'm okay.

S4: 01:18:41.078 Yeah, I was just saying that for this one and kind of every week, I can take the tips of the project, basically, because I think I'm good with going through that throughout the course. Obviously, if anyone is interested with that too, you can jump in, but at least that will be kind of the common [thread?] for me. But I think for this week, that's going to be pretty much it because [inaudible] number on the first week. Bless you. So yeah, that's for me.

S3: 01:19:12.405 Thanks, Loic. See you.

S2: 01:19:15.732 I mean, you all seem to like my idea of doing a file with the information about what project you're joining and stuff. I can write a description out for that activity, assuming that we're doing that for three and not the quiz time graded activity thing, or which one?

S1: 01:19:37.008 I was thinking the three could be, basically make a merge request to a toy project with any content, we don't really care, so that it's easier. And then for the graded thing, we actually do this contribution log project, and.

S2: 01:19:55.331 So then, I can write a description for that. If you want to put me down for that, I will.

S4: 01:20:02.724 Yep. So I think basically that applies to the section that I've taken which is project tips. But that's great because there is plenty to do it there, so you can take care of the tool or the report that crystallized this. And this way I can focus more on the different steps and etc. So if you want to join in on that, that will be a pleasure.

S2: 01:20:27.058 Cool. Sounds good.

S3: 01:20:32.849 Maybe I can help in the technical parts with Marc to setup a personalized GitLab. It will take some time to do that and to beta test everything, so.

S1: 01:20:47.011 Yeah. I can put myself in the first activity which is basically, do something, fork, commit, push, make a merge request.

S3: 01:20:59.085 Right. With me.

S1: 01:21:01.053 Yep. And then there are the contents parts of this module which all the activities are. But the first one is, "What is a software forge?" with the basic notion of version-- one minute on what is a version control system? What is it a project unit? How do you fork it? What is an issue? And the second part, which is how to find useful statistics about projects, like how do you navigate the interface basically, where is information that you will need to find or to quickly see about the project? And the second part, common workflows, which is about push model, pull model, and what is a merge request and what is a code review, basically.

S3: 01:22:00.346 For the 1.1, I could write a script for a video, and then maybe we can do a small video.

S1: 01:22:09.845 We won't go further than the script for the moment. As long as we don't have the IMT contacts for how to actually produce videos and stuff. So writing scripts is, I think, enough for now.

S3: 01:22:28.080 All right, so let me put myself there.

S1: 01:22:30.772 I can go in the second part.

S3: 01:22:36.849 So navigate the forge interface? So GitHub or GitLab?

S1: 01:22:43.435 We should show the two because we can show a parallel between the two. Most of the statistics on one are available on the other.

S3: 01:22:51.940 On GitHub, yes.

S1: 01:22:54.089 I mean, the most important things are if you go to the merge request and the issues and see that there are lots of them and no answers, then it's a bad sign. If you see that all of them have a discussion going on, then it's a good sign that something happening in the a project, and it's not dead.

S2: 01:23:18.743 It's not dead. Yeah, exactly. Not dead yet.

S3: 01:23:24.639 But you have to make sure it's the real source. It's not a fork of a fork of a fork.

S1: 01:23:29.882 There is also an interface where you can see this is a fork of another source project. Okay we assigned everything here, so I think we're good.

S3: 01:23:44.064 Perfect.

S4: 01:23:44.064 I think you have the coordination for the whole week, right, Marc? [inaudible] switch last time.

S1: 01:23:47.875 Oh, yeah. Does anyone wants to coordinate the switch? I can do it.

S2: 01:23:55.445 Probably neither Ildiko nor I because we won't be around.

S1: 01:23:59.000 Remi, maybe.

S3: 01:24:01.209 Okay, yeah.

S4: 01:24:02.566 Well, Remi, before you take too much here, remember you have a bunch of stuff on the first week. Sorry, I'm playing like the first-week coordinator. So maybe just balance that if you can do that before picking more, that would be great too. I mean, if you want to take that, I'm not saying not do, but just keep that in mind, to not over-commit.

S1: 01:24:28.251 You commit twice and then you--

S2: 01:24:31.227 Don't forget to squash.

S1: 01:24:33.715 Yeah.

S3: 01:24:33.715 Yeah.

S4: 01:24:34.349 Yes.

S2: 01:24:36.318 Okay. I got to run to my next meeting, but I will see you all. Yeah. Bye.

S1: 01:24:39.425 Okay. Thank you very much.

S4: 01:24:40.415 All right.

S1: 01:24:42.504 And see you in three weeks.

S2: 01:24:45.066 Yeah. Bye.

S5: 01:24:48.875 I need to go too. I will do my best to do [movies?] and stuff. Thanks. Bye.

S1: 01:24:55.059 Thank you very much. Bye-bye.

S4: 01:24:59.219 Remi, sorry, I didn't mean to cut you on that. If you want to take it, that's all fine. It's just, yeah, keep that in mind.

S3: 01:25:05.190 Yeah. I was just reviewing again.

S1: 01:25:08.201 What you have to do?

S3: 01:25:09.531 Yeah.

S1: 01:25:12.701 And the expected-time column is a bit optimistic on this. I put 30 minutes for fork, clone, commit, and merge requests, which I could do in 5, but I don't think new people not comfortable with the concept can do in 30. Remi, how long do you think it would take to make the first toy contribution just following instructions how to?

S3: 01:25:50.044 I think if we target 30 minutes, that's okay, no more. So it should be simple, straight to the point. No more than 30 minutes total for beginners. It's a good objective, I think.

S1: 01:26:08.315 And then 30 minutes to do it again with specific contents about what you already did for the project?

S3: 01:26:15.454 Yes.

S1: 01:26:20.743 This idea of a project log repository is, I think, great because then in the next tips about the project, or in the next steps where we ask to progress on the contribution, we can just ask to commit to this repository and check that.

S3: 01:26:42.476 Every time they do something, they could commit on their progress log.

S1: 01:26:56.728 Yep. And then this is self--

S3: 01:27:06.755 We have to think about security issues on the GitLab we'll be managing.

S1: 01:27:15.995 Yep. But that's, I think, not bad with GitLab. Okay. You have any more remarks or stuff to add or things we completely missed from the brainstorming and did not want to miss and whoops?

S4: 01:27:39.947 Well, I have a point about that. So first, who is at the end of the coordinator? Is it you for this week, Remi, or you, Marc?

S1: 01:27:52.910 I've put Remi because he's used to teach students about.

S3: 01:27:57.577 Yes. I feel comfortable with this week. So I'm okay with that.

S4: 01:28:04.355 Okay, cool. So then for the brainstorm-- because there is actually a lot of materials that we have from this week obviously but from the previous one. And I found it really useful to go through that and actually try to put it together at least for the first week. And I know that for the section that I'll be doing there, I would love to find kind of the outcome, basically that compilation of the elements that we discussed of the previous brainstorm. So basically what I created for the first week. So I was wondering if the coordinator each week could do that work of going through the previous brainstorm transcript and the noting issues, eventually when there are some, to have that base? And then this way everyone else from that week can just go with that for material and reuse that without having to go through the whole transcript because that's a pretty long thing. And if everyone has to do that for everything that's going to be crazy, I think.

S3: 01:29:07.969 Very nice. Yes. Very good idea.

S1: 01:29:14.758 Okay. So we are at 89 minutes of recording, so it's time to wrap up, I think. So thank you, everyone.

S4: 01:29:25.028 All right. Perfect. Thanks, and see you next week.

S1: 01:29:29.588 And see you next week. Yep.

S3: 01:29:31.825 All right. See you.

S1: 01:29:32.567 All right.

S4: 01:29:33.222 Bye-bye.

S3: 01:29:33.834 See you.
