Marc Jeanmougin:gerrit
Marc Jeanmougin:you can assume gitlab and github are very similar
Marc Jeanmougin:calc: https://lite.framacalc.org/9ml5-mooc-floss
Marc Jeanmougin:Hi loic !
polyedre:https://github.com/octocat/Spoon-Knife Is a sandbox project
Xavier Antoviaque:https://github.com/firstcontributions/first-contributions
Rémi SHARROCK:https://gist.github.com/
Rémi SHARROCK:https://gitlab.com/dashboard/snippets
loic:https://docs.gitlab.com/ce/ssh/#use-ssh-on-microsoft-windows
loic:https://gitlab.com/gitlab-org/gitlab/blob/master/doc/ssh/README.md and contribution is possible
loic:I apologize for assuming the worst from GitLab
Kendall Nelson:I think they are open core, not open source, but I could be wrong.
Marc Jeanmougin:they are
Xavier Antoviaque:https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc/ssh
Xavier Antoviaque:ah you've found it already :) damn, I should read first ;p
polyedre:Brew ?
polyedre:https://git-scm.com/download/mac
Xavier Antoviaque:https://github.com/firstcontributions/first-contributions#first-contributions
Kendall Nelson:https://docs.openstack.org/contributors/common/setup-gerrit.html
Marc Jeanmougin:"use git" is something you can easily google :)
loic:ahaha
Marc Jeanmougin:(you can't amend the commit message in gitlab itself, but you can "apply suggestion" in a commit or MR and that "amends" the commit contents )
loic:oh, interesting
loic:exciting even!
polyedre:Maybe we could add a CI that checks if contributions (like adding a name/pseudo in a file) respects guidelines ?
Rémi SHARROCK:yes that was what we had in mind with the personalized gitlab on our servers :)
Kendall Nelson:Ohhh yeah I like that idea
polyedre:Maybe the CI can grade the contribution ? Each test that passes adds a point for instance
Kendall Nelson:Added a new file: check; there is content in the file: check; passes linters/pep8 etc: check; etc
polyedre:Only one commit; check
Kendall Nelson:+2 :)
loic:+3
Rémi SHARROCK:+4 hehe
Marc Jeanmougin:+5
loic:(that's an OpenStack / gerrit joke of sorts where there are +1 +2 right Kendall ?)
Kendall Nelson:Correct :) +1 and +2 :)
Rémi SHARROCK:+9
Ildiko Vancsa:and +A ;)
loic:I have to run, tty next week!
